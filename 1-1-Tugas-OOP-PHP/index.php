<?php
trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    public function atraksi()
    {
        echo"<br>";
        echo"{$this->nama} sedang {$this->keahlian}";

    }
}
                abstract class Fight{
                    use Hewan;
                    public $attackpower;
                    public $deffencepower;

                    public function serang($hewan)
                    {
                        echo"{$this->nama} sedang menyerang {$hewan->nama}";
                        echo"<br>";
                        $hewan->diserang($this); 
                    }
                    public function diserang($hewan)
                    {
                        echo"{$this->nama} sedang diserang {$hewan->nama}";
                        echo"<br>";

                        $this->darah = $this->darah - ($hewan->attackpower/$this->deffencepower);
                    }
                protected function getInfo()
                {
                    echo"<br>";
                    echo "Nama : {$this->nama}";
                    echo"<br>";
                    echo "darah : {$this->darah}";
                    echo"<br>";
                    echo "Jumlah kaki : {$this->jumlahkaki}";
                    echo"<br>";
                    echo "keahlian : {$this->keahlian}";
                    echo"<br>";
                    echo "attack power : {$this->attackpower}";
                    echo"<br>";
                    echo "deffence power : {$this->deffencepower}";

                    $this->atraksi();    
                }

                abstract public function getInfoHewan();
                }
class Harimau extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackpower = 7;
        $this->deffencepower = 8;
    }
    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau";
        $this->getInfo();
    }

}

class Elang extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackpower = 10;
        $this->deffencepower = 5;
    }
    public function getInfoHewan()
    {
        echo "Jenis Hewan : elang";
        $this->getInfo();
    }

}


class spasi {
    public static function tampilkan(){
        echo "<br>";
        echo "=======================";
        echo "<br>";

    }
}

$Elang = new Elang("elang");
$Elang->getInfoHewan();
spasi::tampilkan();
$harimau = new Harimau("harimau");
$harimau->getInfoHewan();
spasi::tampilkan();
$Elang ->serang($harimau);
spasi::tampilkan();
$harimau->getInfoHewan();
spasi::tampilkan();
$harimau ->serang($Elang);
spasi::tampilkan();
$Elang->getInfoHewan();


