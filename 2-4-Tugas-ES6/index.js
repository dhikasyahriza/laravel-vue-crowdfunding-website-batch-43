// soal 1
console.log("soal 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftarHewanSort = daftarHewan.sort();
for(var i=0; i<daftarHewanSort.length;i++){
    console.log (daftarHewanSort[i])
}

// soal 2
console.log("soal 2");
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
const introduce = (bio)=>{
    return `Nama saya ${bio.name}, umur saya ${bio.age} tahun, alamat saya di ${bio.address}, dan saya punya hobby yaitu ${bio.hobby}`
}
var perkenalan = introduce(data)
console.log (perkenalan)

// soal 3
console.log("soal 3")

const hitung_huruf_vokal= (kata)=> {
    let daftarKataVokal = "aiueo";
    let hitung = 0;

for (var i=0;i<kata.length;i++){
    if(daftarKataVokal.indexOf(kata.toLowerCase()[i]) !==-1)
    {
        hitung += 1;
    }
}
    return hitung
} 

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


//soal 4
console.log("soal 4") 
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    jumlahRute = rute.length
    const angkot= (arrPenumpang)=> {
        let nama = naikAngkot[1];
        let turun = 0;
        let naik = 0;
    // jumlah = rute.join("")
    for(var i=0;i<jumlahRute;i++){
        // menetukan naik
        if (naikAngkot[2] ==rute[i]){
            naik +=1;
        }
        return naik
 }
 for(var a=0;a<jumlahRute;a++){
    // menetukan turun
    if (naikAngkot[3]!==rute[a]){
        turun+=1;
    }
    return turun
}
    var bayar = turun - naik 
    var jumlah = bayar *2000
    return jumlah;

    var naikAngkot = angkot (naik);


    //your code here
  }
}
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]

  //soal 1 ES6
  console.log("soal 1 ES6")
  const luasPersegiPanjang = (l,p)=>{
    return l * p;
  }   
 const luasPersegiPanjang1 = luasPersegiPanjang(5,4);
console.log(luasPersegiPanjang1); 
 

const kelilingPersegiPanjang = (l,p)=>{
    return 2*(p+l);
  }   
 const kelilingPersegiPanjang1 = kelilingPersegiPanjang(6,7);
 console.log(kelilingPersegiPanjang1);
 //soal 2 ES6
 console.log("soal 2 ES6")
 const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: ()=>{
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()

//soal 3 ES6
  console.log("soal 3 ES6")
  const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  const {firstName,lastName,address,hobby}=newObject
  console.log(firstName,lastName,address,hobby )
  //soal 4 ES6
  console.log("soal 4 ES6")
  let west = ["Will", "Chris", "Sam", "Holly"]
  let east = ["Gill", "Brian", "Noel", "Maggie"]
  var combined = west.concat(east)
  //Driver Code
  console.log(combined)

  //soal 5 ES6 
  console.log("soal 5 ES6")
  const planet = "earth" 
  const view = "glass" 
  const string =`Lorem  ${planet} dolor sit amet, consectetur adipiscing elit, ${view} `
  console.log(string)



