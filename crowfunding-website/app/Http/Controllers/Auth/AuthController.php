<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\OtpCode;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Events\userRegisteredEvent;
use App\Events\generateOtp;
use Mail;
use App\Mail\UserRegisteredMail;
use Spatie\Backtrace\File;
use Dotenv\Validator;

class AuthController extends Controller
{
    public function register(Request$request){
        $request->validate([
            'email'=>'required|email|unique:users,email',
            'password'=>'required|confirmed|min:8',
            'name'=>'required'

        ]);
        $user = User::create([
            'email'=>$request['email'],
            'name'=>$request['name'],
            'password'=> Hash::make($request->password),
        ]);

        $data['user'] = $user;
        $token=Auth::login($user);


        event(new userRegisteredEvent($user));
        //Mail::to($user->email)->send(new UserRegisteredMail($user));



        return response ()->json([
            'response_code'=>'00',
            'response_message'=>'data berhasil register',
            'data'=>$data,
            'token'=>$token

        ],201);
    }
    public function login(Request$request){
        $request->validate([
        'email'=>'required',
        'password'=>'required'
        ]);
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['response_message' => 'Unauthorized'], 401);
        }

        return response ()->json([
            'response_code'=>'00',
            'response_message'=>'data berhasil login',
                'token'=>$token
            ]
            ,201);
    }
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => ' log out berhasil']);
    }

    public function profile()
    {
        $data['user'] = Auth()->user()->load('role');
        return response ()->json([
            'response_code'=>'00',
            'response_message'=>'profil berhasil di tampilkan',
                'data'=>$data
            ]
            ,201);
  
    }


    public function updatephotoprofile(Request $request){
        
        $request->validate([
            'name'=>'required',
            'photo_profile'=>'mimes:jpg,jpeg,png',
        ]);
        $id=Auth::id();
        $users = User::find($id);


        if ($request->hasFile('photo_profile')){
            
            //File::delete($image_folder.$request->photo_profile);
            $image=$request->file('photo_profile');
            $image_extension=$image->extension();
            $image_name = time().'.'.$image_extension;
            $image_folder='/photo/post';
            $request->photo_profile->move(public_path($image_folder),$image_name);
            //$image_location=$image_folder.$image_name;

            $update=user::where('id',$id)->update([
                'name'=>$request['name'],
                'photo_profile'=>$request['photo_profile'],
            ]);
        }
        
        $data['user'] = $users;
        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'Berhasil Update photo profile',
           'data'=>[
                'token'=>$users
           ]
            ],201);


    }
    public function updatepassword(Request $request){
        $id=Auth::id();
        $request->validate([
            'email'=>'required',
            'password'=>'required|min:8',
        ]);        
        $ubah = User::find($id);
        $ubah->password = $request['password'];
 
    return response ()->json([
        'response_code'=>'00',
       'response_message'=>'Berhasil Update merubah password',
       
        ],201);
    }

    public function generateOTP(Request $request){
        $request->validate([
            'email'=>'email|required',
        ]);
        $user= User::where('email',$request->email)->first();
        $user->generate_otp_code();
        $data['user']=$user;
        event(new generateOtp($user));

        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'OTP Berhasil di generate',
           'data'=>$data
            ],200); 

    }
    public function verifikasi(Request $request){
        $request->validate([
            'otp'=>'required',
        ]);
        $otp_code = OtpCode::where('otp',$request->otp)->first();

        if(!$otp_code){
            return response ()->json([
                'response_code'=>'01',
               'response_message'=>'OTP tidak ditemukan'
                ],400);
        }
        $now = Carbon::now();

        if($now>$otp_code->valid_until){
            return response ()->json([
                    'response_code'=>'01',
               'response_message'=>'OTP code sudah tidak berlaku, silahkan generate ulang'
                ],400);
        }

        $user = user::find($otp_code->user_id);
        $user ->email_verified_at = $now;
        $user->save();
        $otp_code->delete();

        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'Email di verifikasi'
            ],200);
    }

}
