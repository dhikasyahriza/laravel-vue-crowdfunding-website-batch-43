<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Http\Request;
use File;
class CampaignController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware(['auth'])->except(['show','getAll']);
    // }
     public function index()
    {
        $data['campaign']=Campaign::all();
        if(count($data['campaign'])=== 0){
            return response ()->json([
                'response_code'=>'00',
               'response_message'=>'data masih kosong',
                ],200);            
        }
        else{
            return response ()->json([
                'response_code'=>'00',
               'response_message'=>'Tampil data berhasil',
               'data'=>$data
                ],200);
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'   => 'required',
            'description' => 'required',
            'address' => 'required',
            'image'=>'mimes:jpg,png,jpeg',
            'required' => 'required',
            'collected' => 'required',
        ]);

        $campaign = new Campaign;
        $campaign->title=$request->title;
        $campaign->description=$request->description;
        $campaign->address=$request->address;
        $campaign->required=$request->required;
        $campaign->collected=$request->collected;

        if($request->hasFile('image')){
            $image=$request->file('image');
            //format file
            $image_extention = $image->getClientOriginalExtension();
            //nama uniqe file
            $image_name =time().'.'. $image_extention;
            $image_folder='/photo/campaign/';
            $image_location=$image_folder.$image_name;

            try {
                $image->move(public_path($image_folder),$image_name);
                $campaign->image=$image_location;
                }
                 catch (\Throwable $th) {
                    return response ()->json([
                        'response_code'=>'00',
                       'response_message'=>'Image gagal di tambah',
                        ],400);
            }
        }

        $campaign->save();
        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'Berhasil Menambah Campaign',
           
            ],200);
        }
        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Campaign::findOrFail($id);
        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'Detail Data',
            'data'=>$data    
        ],200);

    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'   => 'required',
            'description' => 'required',
            'address' => 'required',
            'image'=>'mimes:jpg,png,jpeg',
            'required' => 'required',
            'collected' => 'required',
        ]);

        $campaign = Campaign::find($id);
        $campaign->title=$request->title;
        $campaign->description=$request->description;
        $campaign->address=$request->address;
        $campaign->required=$request->required;
        $campaign->collected=$request->collected;

        if($request->hasFile('image')){
            $image=$request->file('image');
            //format file
            $image_extention = $image->getClientOriginalExtension();
            //nama uniqe file
            $image_name =time().'.'. $image_extention;
            $image_folder='/photo/campaign/';
            $sub_campaign=substr($campaign->image,1);
            File::delete($sub_campaign);
            $image_location=$image_folder.$image_name;

            try {
                $image->move(public_path($image_folder),$image_name);
                $campaign->image=$image_location;
                }
                 catch (\Throwable $th) {
                    return response ()->json([
                        'response_code'=>'00',
                       'response_message'=>'Image gagal upload',
                        ],400);
            }
        }

        $campaign->save();
        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'Update data Campign Berhasil',
           
            ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        $sub_campaign=substr($campaign->image,1);
        File::delete($sub_campaign);
        $campaign->delete();
        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'Data berhasil di hapus',
           
            ],200);
    }
    public function getAll(){
        $campaign = Campaign::paginate(2);
        $data['campaign'] =$campaign;
        return response ()->json([
            'response_code'=>'00',
           'response_message'=>'Data berhasil di tampilkan',
           'data'=>$data
            ]);
    }
}
