import { defineStore } from 'pinia'

export const useCampaignStore = defineStore('campaign', {
    state: () => {
      return {
        token: "",
        article: null,
      }
    },
    actions:{
    
        async setCampaign(article, token){
        this.token= token;
        this.user = article;
    },
    },
    
    persist : true
  })