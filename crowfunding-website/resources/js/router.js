import{createRouter,createWebHistory} from 'vue-router'
import { useUserStore } from './store/user'

const Home = () =>import('@/views/Home.vue')
const Campaign = () =>import('@/views/campaign.vue')
const verifikasi = () =>import('@/views/verification.vue')
const DetailCampaign= () =>import('@/views/DetailCampaign.vue')
const router = createRouter({
    history: createWebHistory(),
    routes:[
        {
            path:'/',
            name:'home',
            component: Home
    },
    {
        path:'/campaign/:id',
        name:'campaign-detail',
        component: DetailCampaign
 
},
    {
        path:'/campaign',
        name:'campaign',
        component: Campaign,
            meta:{
            requiredAdmin:true
        }

    },
    {
        path:'/verification',
        name:'verification',
        component: verifikasi,
        meta:{
            requiredVeri:true
        }

    },
    {
        path:'/:catchAll(.*)',
        redirect:'/'
    }
    ] // short for `routes: routes`
  })
  router.beforeEach((to,from)=>{
  const auth = useUserStore()
  if(to.meta.requiredVeri){
  if(!auth.isNotVerification){
    alert("anda tidak memiliki akses di halaman ini")
    return {
        path:'/'
    }
    }
    if(to.meta.requiredAdmin){
        if(!auth.isAdmin){
          alert("Halaman ini hanya bisa di akses oleh admin")
          return {
              path:'/'
          }
          }
  }
  }
}
  )

  export default router