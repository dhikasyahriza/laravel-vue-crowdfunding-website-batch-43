<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CampaignController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'prefix' => 'auth',
], function(){
    Route::post('/register',[AuthController::class,'register']);
    Route::post('/login',[AuthController::class,'login']);
    Route::post('/generate-otp-code',[AuthController::class,'generateOTP']);
    Route::post('/verification-email',[AuthController::class,'verifikasi']);
    Route::post('/logout',[AuthController::class,'logout'])->middleware('auth');
    Route::post('/update-password',[AuthController::class,'updatepassword'])->middleware('auth');
    
}
);
Route::get('/get-profile',[AuthController::class,'profile'])->middleware('auth');
Route::post('/update-profile',[AuthController::class,'updatephotoprofile'])->middleware('auth');

route::get('test',function(){
    return"berhasil";
})->middleware('api');


route::apiResource('campaign',campaignController::class);
Route::get('/getallcampaign',[campaignController::class,'getAll']);